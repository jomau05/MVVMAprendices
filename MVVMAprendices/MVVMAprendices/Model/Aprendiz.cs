﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMAprendices.Model
{
    using MVVMAprendices.ModelView;
    public class Aprendiz : Notificable
    {
        #region Atributos
        private string nombre;
        private string apellido;
        private double ficha;
        private string programa;
        private double promedio;
        #endregion

        #region Propiedades


        public string Nombre
        {
            get { return nombre; }
            set
            {
                if (nombre != value)
                {
                    nombre = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Apellido
        {
            get { return apellido; }
            set
            {
                if (apellido != value)
                {
                    apellido = value;
                    OnPropertyChanged();
                }
            }
        }

        public double Ficha
        {
            get { return ficha; }
            set
            {
                if (ficha != value)
                {
                    ficha = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Programa
        {
            get { return programa; }
            set
            {
                if (programa != value)
                {
                    programa = value;
                    OnPropertyChanged();
                }
            }
        }

        public double Promedio
        {
            get { return promedio; }
            set
            {
                if (promedio != value)
                {
                    promedio = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion
    }
}
