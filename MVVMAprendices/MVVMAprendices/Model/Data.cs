﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMAprendices.Model
{
    using MVVMAprendices.ModelView;
    using System.Collections.ObjectModel;
   public class Data:Notificable
    {
        private ObservableCollection<Aprendiz> aprendices = new ObservableCollection<Aprendiz>();

        public ObservableCollection<Aprendiz> Aprendices
        {
            get { return aprendices; }
            set {
                if (aprendices != value)
                {
                    aprendices = value;
                    OnPropertyChanged();
                }
            }
        }

    }
}
