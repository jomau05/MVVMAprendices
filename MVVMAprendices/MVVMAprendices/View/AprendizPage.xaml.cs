﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMAprendices.View
{
    using Xamarin.Forms;
    using ModelView;
    using Model;
    using View;
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AprendizPage : ContentPage
	{
		public AprendizPage ()
		{
			InitializeComponent ();
            this.BindingContext = new AprendizPageViewModel();
            this.lstName.ItemSelected += LstName_ItemSelected;
		}

        private void LstName_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var aprendizSeleccionado = (Aprendiz)e.SelectedItem;
            if (aprendizSeleccionado != null) {
                Navigation.PushAsync(new DetalleAprendizPage(aprendizSeleccionado));
                lstName.SelectedItem = null;
            }
        }
    }
}