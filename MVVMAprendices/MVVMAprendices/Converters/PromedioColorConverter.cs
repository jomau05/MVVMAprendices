﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMAprendices.Converters
{
    using System.Globalization;
    using Xamarin.Forms;
    class PromedioColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var promedio = (double)value;
            return promedio < 500 ? Color.Red : Color.Green;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
