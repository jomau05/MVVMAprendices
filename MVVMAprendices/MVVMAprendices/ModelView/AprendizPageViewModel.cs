﻿

namespace MVVMAprendices.ModelView
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MVVMAprendices.Model;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using Xamarin.Forms;
    using Model.Services;

    public class AprendizPageViewModel : Notificable
    {

        #region Atributos
        private ObservableCollection<Aprendiz> aprendices;
        private bool isEnabled;
        #endregion

        #region Propiedades
        public ObservableCollection<Aprendiz> Aprendices
        {
            get { return aprendices; }
            set
            {
                if (aprendices != value)
                {
                    aprendices = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public Command CargarAprendizCommand { get; set; }
        #endregion

        #region Contructores
        public AprendizPageViewModel()
        {
            Aprendices = new ObservableCollection<Aprendiz>();
            CargarAprendizCommand = new Command((obj) => CargarAprendices());

        }
        #endregion

        #region Métodos
        private async void CargarAprendices()
        {
            if (!IsEnabled)
            {
                IsEnabled = true;
                await Task.Delay(3000);
                Data listAprendices = CaracterizacionAprendices.CargarAprendices();
                Aprendices = listAprendices.Aprendices;
                IsEnabled = false;

            }
        }
        #endregion

    }
}
